package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"github.com/codegangsta/cli"
	"github.com/golang/protobuf/proto"
)

func fail(msg string) {
	fmt.Fprintln(os.Stderr, msg)
	os.Exit(1)
}

func failf(format string, args ...interface{}) {
	fail(fmt.Sprintf(format, args...))
}

// findOneFile finds all files in the specified directory which
// match the pattern, and only succeeds if the file is just one,
// and it's a regular file or a symlink to a regular file.
func findOneFile(dir, pattern string) (fname string, err error) {
	f, err := os.Open(dir)
	if err != nil {
		return
	}
	defer f.Close()
	names, err := f.Readdirnames(-1)
	if err != nil {
		return
	}
	for _, name := range names {
		if ok, err := path.Match(pattern, name); ok && err == nil {
			if fname != "" {
				return "", fmt.Errorf("multiple files which match the pattern %q found in %s. For example, %s and %s", pattern, dir, fname, name)
			}
			fname = name
		}
	}
	if fname == "" {
		return "", fmt.Errorf("no files match the pattern %q found in %s", pattern, dir)
	}
	fname = path.Join(dir, fname)
	fi, err := os.Stat(fname)
	if err != nil {
		return
	}
	if !fi.Mode().IsRegular() {
		return "", fmt.Errorf("%s does not point to a regular file", fname)
	}
	return
}

func readFileOrDie(fname string) []byte {
	data, err := ioutil.ReadFile(fname)
	if err != nil {
		failf("Failed to read the data from %s: %v", fname, err)
	}
	return data
}

func blobsString(blobs []*caffe.BlobProto) string {
	var res []string
	for _, blob := range blobs {
		var tmp caffe.BlobProto = *blob
		tmp.Data = nil
		res = append(res, proto.CompactTextString(&tmp))
	}
	return "[" + strings.Join(res, ",\n") + "]"
}

// upgradeBlob upgrades caffe.BlobProto in place.
func upgradeBlob(in *caffe.BlobProto) error {
	if in.Shape != nil {
		if in.Num != nil || in.Channels != nil || in.Height != nil || in.Width != nil {
			return errors.New("both shape and num / channels / height / width are set")
		}
	} else {
		in.Shape = &caffe.BlobShape{
			// See https://github.com/BVLC/caffe/blob/fc77ef3d23423e57d83e16c32844bbbe8d4d9f0c/src/caffe/blob.cpp#L440
			Dim: []int64{int64(in.GetNum()), int64(in.GetChannels()), int64(in.GetHeight()), int64(in.GetWidth())},
		}
		in.Num = nil
		in.Channels = nil
		in.Height = nil
		in.Width = nil
	}
	return nil
}

// upgradeLayer upgrades caffe.LayerParameter in place.
func upgradeLayer(in *caffe.LayerParameter) error {
	for i, blob := range in.Blobs {
		if err := upgradeBlob(blob); err != nil {
			return fmt.Errorf("blob #%d: %v", i, err)
		}
	}
	return nil
}

// upgradeNetParameter upgrades caffe.NetParameter in place.
func upgradeNetParameter(in *caffe.NetParameter) error {
	for i, v := range in.InputShape {
		if v == nil {
			return fmt.Errorf("input shape #%d is empty", i)
		}
		if len(v.Dim) != 4 {
			return fmt.Errorf("input shape #%d has dimension %d, want 4", i, len(v.Dim))
		}
		// Expect a single image
		v.Dim[0] = 1
	}
	for _, layer := range in.Layer {
		if err := upgradeLayer(layer); err != nil {
			return fmt.Errorf("layer %s: %v", layer.GetName(), err)
		}
	}
	return nil
}

func copyWeights(to, from *caffe.NetParameter) error {
	toLayers := map[string]*caffe.LayerParameter{}
	for _, layer := range to.Layer {
		switch layer.GetType() {
		case "Convolution", "InnerProduct":
			toLayers[layer.GetName()] = layer
		case "LRN", "Softmax", "Dropout", "ReLU", "Concat", "Pooling":
		default:
			failf("Unsupported layer type: %s", layer.GetType())
		}
	}
	fmt.Printf("len(from.Layer) = %d\n", len(from.Layer))
	fmt.Printf("len(from.Layers) = %d\n", len(from.Layers))
	for _, layer := range from.Layer {
		toLayer := toLayers[layer.GetName()]
		if toLayer == nil {
			if len(layer.Blobs) > 0 {
				fmt.Printf("layer not found in dest: %s, %s, while src has %d blobs\n",
					layer.GetName(), layer.GetType(), len(layer.Blobs))
			}
			// It's OK. Just skip extra layers.
			continue
		}
		delete(toLayers, layer.GetName())
		if layer.GetType() != toLayer.GetType() {
			return fmt.Errorf("layer %s has different types in source and destination: %s vs %s",
				layer.GetType(), toLayer.GetType())
		}
		if len(layer.Blobs) != len(toLayer.Blobs) {
			return fmt.Errorf("layer %s : %s has inconsistent number of blobs: from: %d, to: %d\nfrom: %s\nto:   %s",
				layer.GetName(), layer.GetType(), len(layer.Blobs), len(toLayer.Blobs),
				blobsString(layer.Blobs), blobsString(toLayer.Blobs))
		}
		fmt.Printf("Copy: layer %s, len(layer.Blobs): %d\n", layer.GetName(), len(layer.Blobs))
		for i, blob := range layer.Blobs {
			if blob == nil {
				return fmt.Errorf("source layer %s does not have blob[%d] set", layer.GetName(), i)
			}
			if toLayer.Blobs[i] == nil {
				return fmt.Errorf("dest layer %s does not have blob[%d] set", layer.GetName(), i)
			}
			fmt.Printf("layer %s, blob #%d, from: %s, to: %s\n", layer.GetName(), i,
				blob.GetShape(), toLayer.Blobs[i].GetShape())
			if !proto.Equal(blob.GetShape(), toLayer.Blobs[i].GetShape()) {
				return fmt.Errorf("layer %s, blob #%d has inconsistent shapes:\nfrom: %v\nvs\nto:   %v",
					layer.GetName(), i, blob.GetShape(), toLayer.Blobs[i].GetShape())
			}
			toLayer.Blobs[i] = blob
		}
	}
	if len(toLayers) != 0 {
		var names []string
		for _, layer := range toLayers {
			names = append(names, layer.GetName())
		}
		return fmt.Errorf("weights for some layers have not been found in the source: %q", names)
	}
	return nil
}

func computeDataShapesLayer(in *caffe.LayerParameter, shapes map[string][]int64) error {
	fmt.Printf("Layer %s: %s\n", in.GetName(), in.GetType())
	if len(in.Bottom) == 0 {
		return errors.New("no bottom blobs")
	}
	if len(in.Top) == 0 {
		return errors.New("no top blobs")
	}
	if len(in.Top) > 1 {
		return fmt.Errorf("layer type %s expected exactly 1 top, but %d declared", in.GetType(), len(in.Top))
	}
	// First, consider the layer types, which deal with multiple tops or bottoms.
	if in.GetType() == "Concat" {
		// Here we assume that all shapes are 4D.
		axis := in.ConcatParam.GetAxis() % 4
		if axis < 0 {
			axis += 4
		}
		switch axis {
		case 0, 1:
			fmt.Printf("Concat bottoms: %v\n", in.Bottom)
			var sum int64
			// TODO: check that all bottoms could be found in the beginning of the function
			h := shapes[in.Bottom[0]][2]
			w := shapes[in.Bottom[0]][3]
			for i, name := range in.Bottom {
				shape := shapes[name]
				fmt.Printf("Bottom #%d: %v\n", i, shape)
				if shape == nil {
					return fmt.Errorf("could not find bottom blob %s", name)
				}
				if shape[2] != h || shape[3] != w {
					return fmt.Errorf("bottom blob %s and bottom blob %s have inconsitent height / width: (%d, %d) vs (%d, %d)",
						in.Bottom[0], in.Bottom[i], h, w, shape[2], shape[3])
				}
				if axis == 0 {
					sum += shape[0]
				} else {
					sum += shape[1]
				}
			}
			if axis == 0 {
				shapes[in.Top[0]] = []int64{sum, shapes[in.Bottom[0]][1], h, w}
			} else {
				shapes[in.Top[0]] = []int64{shapes[in.Bottom[0]][0], sum, h, w}
			}
		default:
			return fmt.Errorf("axis=%d is not supported for Concat layer type", axis)
		}
		return nil
	}
	// Now, we're left with layers which have one top and one bottom.
	if len(in.Bottom) > 1 {
		return fmt.Errorf("layer type %s expects exactly 1 bottom, but %d declared", in.GetType(), len(in.Bottom))
	}
	bottom := shapes[in.Bottom[0]]
	if bottom == nil {
		return fmt.Errorf("could not find bottom blob %s", bottom)
	}
	switch in.GetType() {
	case "Convolution":
		if in.ConvolutionParam == nil {
			return fmt.Errorf("convolution_param is not set")
		}
		cp := in.GetConvolutionParam()
		numOutput := int64(cp.GetNumOutput())
		var padH, padW, kernelH, kernelW, strideH, strideW int64
		if cp.GetPad() != 0 {
			padH = int64(cp.GetPad())
			padW = int64(cp.GetPad())
		} else {
			padH = int64(cp.GetPadH())
			padW = int64(cp.GetPadW())
		}
		if cp.GetKernelSize() != 0 {
			kernelH = int64(cp.GetKernelSize())
			kernelW = int64(cp.GetKernelSize())
		} else {
			kernelH = int64(cp.GetKernelH())
			kernelW = int64(cp.GetKernelW())
		}
		if cp.GetStride() != 0 {
			strideH = int64(cp.GetStride())
			strideW = int64(cp.GetStride())
		} else {
			strideH = int64(cp.GetStrideH())
			strideW = int64(cp.GetStrideW())
		}
		if strideH == 0 {
			strideH = 1
		}
		if strideW == 0 {
			strideW = 1
		}
		height := (bottom[2]+2*padH-kernelH)/strideH + 1
		width := (bottom[3]+2*padW-kernelW)/strideW + 1
		shapes[in.Top[0]] = []int64{bottom[0], numOutput, height, width}
	case "ReLU", "LRN", "Dropout":
		shapes[in.Top[0]] = bottom
	case "Pooling":
		if in.PoolingParam == nil {
			return fmt.Errorf("convolution_param is not set")
		}
		pp := in.GetPoolingParam()
		var padH, padW, kernelH, kernelW, strideH, strideW int64
		if pp.GetPad() != 0 {
			padH = int64(pp.GetPad())
			padW = int64(pp.GetPad())
		} else {
			padH = int64(pp.GetPadH())
			padW = int64(pp.GetPadW())
		}
		if pp.GetKernelSize() != 0 {
			kernelH = int64(pp.GetKernelSize())
			kernelW = int64(pp.GetKernelSize())
		} else {
			kernelH = int64(pp.GetKernelH())
			kernelW = int64(pp.GetKernelW())
		}
		if pp.GetStride() != 0 {
			strideH = int64(pp.GetStride())
			strideW = int64(pp.GetStride())
		} else {
			strideH = int64(pp.GetStrideH())
			strideW = int64(pp.GetStrideW())
		}
		if strideH == 0 {
			strideH = 1
		}
		if strideW == 0 {
			strideW = 1
		}
		height := (bottom[2]+2*padH-kernelH)/strideH + 1
		width := (bottom[3]+2*padW-kernelW)/strideW + 1
		// TODO: understand why does it even happen with bvlc_googlenet.
		if height <= 0 {
			fmt.Printf("WARNING: height: %d -> 1\n", height)
			height = 1
		}
		if width <= 0 {
			fmt.Printf("WARNING: width: %d -> 1\n", width)
			width = 1
		}
		if height == 0 || width == 0 {
			return fmt.Errorf("invalid height / width: %d / %d for Pooling layer with bottom: %v, padH: %d, kernelH: %d, strideH: %d, padW: %d, kernelW: %d, strideW: %d",
				height, width, bottom, padH, kernelH, strideH, padW, kernelW, strideW)
		}
		shapes[in.Top[0]] = []int64{bottom[0], bottom[1], height, width}
		fmt.Printf("Pooling %s: height: %d, width: %d, bottom: %v, padH: %d, kernelH: %d, strideH: %d, padW: %d, kernelW: %d, strideW: %d\n",
			in.GetName(), height, width, bottom, padH, kernelH, strideH, padW, kernelW, strideW)
	case "InnerProduct":
		axis := in.GetInnerProductParam().GetAxis()
		if axis != 1 {
			return fmt.Errorf("axis=%d is not supported for InnerProduct layer type", axis)
		}
		n := int64(in.GetInnerProductParam().GetNumOutput())
		shapes[in.Top[0]] = []int64{bottom[0], n, 1, 1}
	case "Softmax":
		axis := in.GetInnerProductParam().GetAxis()
		if axis != 1 {
			return fmt.Errorf("axis=%d is not supported for Softmax layer type", axis)
		}
		shapes[in.Top[0]] = []int64{bottom[0], bottom[1], bottom[2], bottom[3]}
		fmt.Printf("SOFTMAX layer %s, bottom: %v, top: %v\n", in.GetName(), bottom, shapes[in.Top[0]])
	default:
		return fmt.Errorf("layer type %s is not supported", in.GetType())
	}
	fmt.Printf("%s layer %s, bottom: %v, top: %v\n", in.GetType(), in.GetName(), bottom, shapes[in.Top[0]])
	return nil
}

func computeDataShapes(in *caffe.NetParameter) (map[string][]int64, error) {
	if len(in.Input) == 0 {
		return nil, errors.New("net has not inputs")
	}
	if len(in.InputShape) != len(in.Input) {
		return nil, fmt.Errorf("number of inputs (%d) does not match the number of input shapes (%d)",
			len(in.InputShape), len(in.Input))
	}

	// Add input shapes
	shapes := map[string][]int64{}
	for i, shape := range in.InputShape {
		shapes[in.Input[i]] = shape.Dim
	}
	// Now, go a layer after a layer.
	// We assume that layers are topologically sorted.
	for _, layer := range in.Layer {
		if err := computeDataShapesLayer(layer, shapes); err != nil {
			return nil, fmt.Errorf("layer %s: %v", layer.GetName(), err)
		}
	}
	return shapes, nil
}

func initBlobsLayer(in *caffe.LayerParameter, dataShapes map[string][]int64) error {
	fmt.Printf("%s: %s\n", in.GetName(), in.GetType())
	if len(in.Blobs) != 0 {
		return fmt.Errorf("blobs already initialized: %s", blobsString(in.Blobs))
	}
	if len(in.Bottom) == 0 {
		return errors.New("no bottom blobs")
	}
	bottom := dataShapes[in.Bottom[0]]
	switch in.GetType() {
	case "Convolution":
		cp := in.GetConvolutionParam()
		numOutput := int64(cp.GetNumOutput())
		group := int64(cp.GetGroup())
		if group == 0 {
			return fmt.Errorf("invalid convolution_param: group == 0")
		}
		if numOutput%group != 0 {
			return fmt.Errorf("invalid convolution_param: could not split %d channels into %d groups",
				numOutput, group)
		}
		if bottom[1]%group != 0 {
			return fmt.Errorf("invalid convolution_param: could not split %d inputs into %d groups",
				bottom[1], group)
		}
		var kernelH, kernelW int64
		if cp.GetKernelSize() != 0 {
			kernelH = int64(cp.GetKernelSize())
			kernelW = int64(cp.GetKernelSize())
		} else {
			kernelH = int64(cp.GetKernelH())
			kernelW = int64(cp.GetKernelW())
		}
		in.Blobs = []*caffe.BlobProto{
			{Shape: &caffe.BlobShape{Dim: []int64{numOutput, bottom[1] / group, kernelH, kernelW}}},
			{Shape: &caffe.BlobShape{Dim: []int64{1, 1, 1, numOutput}}},
		}
	case "ReLU", "Pooling", "LRN", "Concat", "Dropout":
	case "InnerProduct":
		ip := in.GetInnerProductParam()
		numOutput := int64(ip.GetNumOutput())
		in.Blobs = []*caffe.BlobProto{
			{Shape: &caffe.BlobShape{Dim: []int64{1, 1, numOutput, bottom[1] * bottom[2] * bottom[3]}}},
			{Shape: &caffe.BlobShape{Dim: []int64{1, 1, 1, numOutput}}},
		}
	case "Softmax":
	default:
		return fmt.Errorf("unsupported layer type: %s", in.GetType())
	}
	// TODO: implement other layer types.
	return nil
}

// initBlobs initializes blobs with proper shapes, but keeps the data empty.
func initBlobsNetParameter(in *caffe.NetParameter, dataShapes map[string][]int64) error {
	fmt.Println("Initializing weight blobs")
	for _, layer := range in.Layer {
		if err := initBlobsLayer(layer, dataShapes); err != nil {
			return fmt.Errorf("layer %s: %v", layer.GetName(), err)
		}
	}
	return nil
}

func readStrings(fname string) ([]string, error) {
	data, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}
	return strings.Split(string(data), "\n"), nil
}

func classifyCmd(ctx *cli.Context) {
	dir := ctx.Args().First()
	// Check args
	if dir == "" {
		fail("Please, specify a directory with a Caffe model.")
	}
	if len(ctx.Args().Tail()) != 3 {
		fail("Invalid number of parameters for goffe classify")
	}
	meanFname := ctx.Args().Get(1)
	if meanFname == "" {
		fail("Path to mean is not set")
	}
	labelsFname := ctx.Args().Get(2)
	if labelsFname == "" {
		fail("Path to labels is not set")
	}
	imgFname := ctx.Args().Get(3)
	if imgFname == "" {
		fail("Path to image is not set")
	}
	// Check that dir is a valid directory
	fi, err := os.Stat(dir)
	if err != nil {
		failf("%s is not a directory or could not be accessed: %v", dir, err)
	}
	if !fi.IsDir() {
		failf("%s is not a directory", dir)
	}
	// Locate the net and weights files
	netFname, err := findOneFile(dir, "deploy.prototxt")
	if err != nil {
		failf("Could not locate deploy.prototxt file in %s: %v", dir, err)
	}
	weightsFname, err := findOneFile(dir, "*.caffemodel")
	if err != nil {
		failf("Could not locate a caffemodel file in %s: %v", dir, err)
	}
	fmt.Printf("Net: %s\n", netFname)
	fmt.Printf("Weights: %s\n", weightsFname)
	// Read both files
	var net caffe.NetParameter
	if err := proto.UnmarshalText(string(readFileOrDie(netFname)), &net); err != nil {
		failf("Failed to parse the net from %s: %v", netFname, err)
	}
	var weights caffe.NetParameter
	if err := proto.Unmarshal(readFileOrDie(weightsFname), &weights); err != nil {
		failf("Failed to parse the weights from %s: %v", weightsFname, err)
	}
	// Upgrade protos
	// TODO: probably, a Caffe tool should be responsible for that.
	// Right now, just hack something quick.
	if err := upgradeNetParameter(&net); err != nil {
		failf("Failed to upgrade the net from %s: %v", netFname, err)
	}
	if err := upgradeNetParameter(&weights); err != nil {
		failf("Failed to upgrade the weights from %s: %v", weightsFname, err)
	}
	dataShapes, err := computeDataShapes(&net)
	if err != nil {
		failf("Failed to compute data shapes from %s: %v", netFname, err)
	}
	// Initialize the net blobs with empty data. Later, we'll copy it from weights.
	if err := initBlobsNetParameter(&net, dataShapes); err != nil {
		failf("Failed to initialize blobs for the net from %s: %v", netFname, err)
	}
	// Copy weights
	// TODO: probably, this should be a responsibility of a C++ utility.
	// On Go side, we should have a single file, which is known to be correctly merged.
	if err := copyWeights(&net, &weights); err != nil {
		failf("Failed to copy weights from %s to %s: %v\n", weightsFname, netFname, err)
	}
	fmt.Println("Both files parsed successfully!")
	// Load labels
	labels, err := readStrings(labelsFname)
	if err != nil {
		failf("Failed to load labels from %s: %v", labelsFname, err)
	}
	// Load image blob
	img, err := gf.LoadImage(imgFname)
	if err != nil {
		failf("Failed to load image from %s: %v", imgFname, err)
	}
	fmt.Printf("Loaded image dim: %v, L1: %f\n", img.Dim, img.L1())
	// Load mean binary proto
	mean, err := gf.LoadBinaryBlob(meanFname)
	if err != nil {
		failf("Failed to load mean binary proto from %s: %v", meanFname, err)
	}
	// Compute per-channel mean.
	mean = mean.Mean(1)
	fmt.Printf("mean: %v\n", mean)
	// Image net mean: 104.007, 116.669, 122.679
	for y := int64(0); y < img.Dim[2]; y++ {
		for x := int64(0); x < img.Dim[3]; x++ {
			img.Data[img.Idx(0, 0, y, x)] -= mean.Data[mean.Idx(0, 0, 0, 0)]
			img.Data[img.Idx(0, 1, y, x)] -= mean.Data[mean.Idx(0, 1, 0, 0)]
			img.Data[img.Idx(0, 2, y, x)] -= mean.Data[mean.Idx(0, 2, 0, 0)]
		}
	}
	fmt.Printf("Preprocessed image, L1: %f, L2: %f\n", img.L1(), img.L2())
	//fmt.Printf("b[0,0,0,0]: %f, b[0,0,1,1]: %f\n", img.At(0, 0, 0, 0), img.At(0, 0, 1, 1))
	fmt.Printf("b[0,0]:\n")
	for y := int64(0); y < 10; y++ {
		for x := int64(0); x < 10; x++ {
			fmt.Printf("%6.2f ", img.At(0, 0, y, x))
		}
		fmt.Println()
	}

	state := map[string]*gf.Blob{"data": img}
	if err := gf.Generate("GoogLeNet.gen.metal", "GoogLeNet.gen.swift", "GoogLeNet.data", &net, state); err != nil {
		failf("Failed to generate a forward pass: %v", err)
	}
	//if err := gf.Forward(&net, state); err != nil {
	//	failf("Failed to compute a forward pass: %v", err)
	//}
	fmt.Println("Forward pass succeeded")

	// HACK: find the answer
	for i := 0; i < 5; i++ {
		idx, p := argMax(state["prob"].Data)
		state["prob"].Data[idx] = 0
		fmt.Printf("%d. %s - %f\n", i, labels[idx], p)
	}
}

func argMax(data []float32) (idx int, max float32) {
	for i, v := range data {
		if v > max {
			max = v
			idx = i
		}
	}
	return
}

func main() {
	app := cli.NewApp()
	app.Name = "goffe"
	app.Usage = "A tool to work with Caffe models"
	app.Commands = []cli.Command{
		{
			Name:   "classify",
			Usage:  "classify <network_dir> <mean.binaryproto> <labels.txt> <img.jpg>",
			Action: classifyCmd,
		},
		{
			Name:    "hello",
			Aliases: []string{"hi"},
			Usage:   "say hello",
			Action: func(c *cli.Context) {
				println("hello!")
			},
		},
	}

	app.Run(os.Args)
}
